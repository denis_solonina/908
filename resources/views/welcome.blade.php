<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
</head>
<body>

<form action="/load-xml" method="post" enctype="multipart/form-data">
    @csrf
    <input type="file" name="xml_doc" id="xml_doc_id">
    <input type="submit" value="Upload" name="submit">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">


</form>

</body>

</html>
