<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Founder extends Model
{
    protected $table = 'founders';

    protected $fillable = ['founder', 'edr_uo_id'];


    public function edr()
    {
        return $this->belongsTo(Edr::class, 'id', 'edr_uo_id');
    }
}
