<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 03.02.19
 * Time: 16:48
 */

namespace App\Http\Services;

//use XMLReader;
use App\Edr;

/**
 * Class XmlService
 * @package App\Http\Services
 */
class XmlService
{
    /**
     * @var \XMLReader
     */
    private $xmlReader;
    /**
     * @var \DOMDocument
     */
    private $document;


    /**
     * XmlService constructor.
     */
    public function __construct()
    {
        $this->xmlReader = new \XMLReader();
        $this->document = new \DOMDocument();
    }

    /**
     * @param string $path
     * @return void
     */
    public function parseXml(string $path): void
    {
        $this->xmlReader->open($path);
        while ($this->xmlReader->read() && $this->xmlReader->name !== 'RECORD') ;
        while ($this->xmlReader->name === 'RECORD') {
            $this->createRowInDb(simplexml_import_dom($this->document
                ->importNode($this->xmlReader->expand(), true)));
            $this->xmlReader->next('RECORD');
        }
    }

    /**
     * @param $dataRow
     */
    public function createRowInDb($dataRow): void
    {
        $edr = new Edr();
        foreach (Edr::FIELD_NAME as $fieldNameXml => $fieldNameDb) {
            $edr->$fieldNameDb = $dataRow->$fieldNameXml;
        }
        $edr->save();
        if ($dataRow->FOUNDERS) {
            foreach ($dataRow->FOUNDERS->FOUNDER as $founder) {
                $edr->founder()->create(['founder' => $founder]);
            }
        }
        unset($edr);
    }

    /**
     * @param $adress
     * @return array
     */
    public function parseStringToJson($adress)
    {
        $entity = [];
        $entity['place'] = $patternsCity = [
            '/м\.{0,1}\s?([А-Я]+[А-Яа-яіЇІїҐґCcЬьсРрВвТт]{1,15})/',
            '/місто\s?([А-Я]{1}[А-Яа-яіЇІїҐґCcЬьсРрВвТт]{1,15})/',
        ];

        $entity['region'] = $paternsRegiont = [
            '/([А-Яа-яіЇІїҐґCcЬьсРрВвТт]+)\sобл\.?/i',
            '/([А-Яа-яіЇІїҐґCcЬьсРрВвТт]+)\sобласть\.?/i',
            '/([А-Яа-яіЇІїҐґCcЬьсРрВвТт]+)\sОбласть\.?/i',
            '/([А-Яа-яіЇІїҐґCcЬьсРрВвТт]+)\sОБЛ\.?/i',
            '/([А-Яа-яіЇІїҐґCcЬьсРрВвТт]+)\sОбл\.?/i',
            '/([А-Яа-яіЇІїҐґCcЬьсРрВвТт]+)\sОБЛАСТЬ\.?/i',
        ];

        $entity['district'] = $paternsDistrict = [
            '/([А-Яа-яіЇІїҐґCcЬьсРрВвТт]+)\sр\.?/i',
            '/([А-Яа-яіЇІїҐґCcЬьсРрВвТт]+)\sР\.?/i',
            '/([А-Яа-яіЇІїҐґ]+)\s{0,1}район\.?/i',
            '/([А-Яа-яіЇІїҐґCcЬьсРрВвТт]+)\sРайон\.?/i',
            '/([А-Яа-яіЇІїҐґCcЬьсРрВвТт]+)\sРАЙОН\.?/i',
        ];


        $entity['street'] = $paternsStreet = [
            '/ВУЛИЦЯ.{0,1}\s([А-Яа-яіЇІїҐґCcЬьсРрВвТт\/\-]+)/i',
            '/вулиця\s{1}([А-Яа-яіЇІїҐґCcЬьсРрВвТтОо\/\-]+)/i',
            '/Вулиця\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт\/\-]+)/i',
            '/ПЛОЩА\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт\/\-]+)/i',
            '/площа\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт\/\-]+)/i',
            '/Площа\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт\/\-]+)/i',
            '/вул\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт\/\-]+)/i',
            '/ВУЛ\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт\/\-]+)/i',

        ];


        $entity['house'] = $paternsHouse = [
            '/будинок\s([А-Яа-яіЇІїҐґCcЬьсРрВвТт1-9\-\/]+)/i',
            '/Будинок\s{1}([А-Яа-яіЇІїҐґCcЬьсРрВвТтОо1-9\-\/]+)/i',
            '/БУДИНОК\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт1-9\-\/]+)/i',
            '/буд\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт1-9\-\/]+)/i',
            '/Буд\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт1-9\-\/]+)/i',
            '/БУД\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт1-9\-\/]+)/i',

        ];

        $entity['number'] = $paternsNumber = [
            '/офіс\s([А-Яа-яіЇІїҐґCcЬьсРрВвТт1-9\-]+)/i',
            '/квартира\s{1}([А-Яа-яіЇІїҐґCcЬьсРрВвТтОо1-9\-]+)/i',
            '/Квартира\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт1-9\-]+)/i',
            '/кв\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт1-9\-]+)/i',
            '/КВ\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт1-9\-]+)/i',
            '/Кв\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт1-9\-]+)/i',
            '/оф\.{0,1}\s?([А-Яа-яіЇІїҐґCcЬьсРрВвТт1-9\-]+)/i',

        ];

        $response = [];
        foreach ($entity as $key => $entityPaterns) {
            foreach ($entityPaterns as $patern) {
                preg_match($patern, $adress, $result);
                if ($result) {
                    $response[$key] = $result[1];
                    break;
                }
            }
        }
        return $response;
    }
}
