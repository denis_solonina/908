<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 03.02.19
 * Time: 12:39
 */

namespace App\Http\Controllers;

use App\Edr;
use \Illuminate\Http\Request;

/**
 * Class EdrController
 * @package App\Http\Controllers
 */
class EdrController
{
    /**
     * @param Request $request
     * @return string
     */
    public function uploadFile(Request $request)
    {
        $pathToXml = $request->xml_doc; // path to temp file name
        if (!$pathToXml) {
        return response()->json(['message' => 'File not upload']);
        }
        try {
            app('XmlService')->parseXml($pathToXml);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findEntity(Request $request)
    {
        $adress = $request->get('adress', null);
        return response()->json(['parts' => app('XmlService')->parseStringToJson($adress)]);
    }
}
