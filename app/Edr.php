<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Edr extends Model
{

    const FIELD_NAME = [
        'NAME' => 'name',
        'SHORT_NAME' => 'short_name',
        'EDRPOU' => 'edrpou',
        'ADDRESS' => 'adress',
        'BOSS' => 'boss',
        'KVED' => 'kved',
        'STAN' => 'stan',
    ];

    protected $table = 'edr_uo';

    public function founder()
    {
        return $this->hasMany(Founder::class, 'edr_uo_id', 'id');
    }
}
